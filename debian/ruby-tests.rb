$: << 'lib' << '.'

if not STDOUT.isatty or not STDIN.isatty
  puts "STDIN or STDOUT not a tty, cannot run tests."
  exit(0)
end
require 'test/test0.rb'
